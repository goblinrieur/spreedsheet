#! /usr/bin/env bash

filename="$1"
runner=$(which gforth-fast)

if [[ ${runner} != *"gforth-fast"* ]] ; then 
	 echo "Gforth interpreter not found"
	 exit 1
fi
trap '' SIGINT
$runner ./mini-spreadsheet.fs $filename
clear 
trap SIGINT
if [ "$filename" = "cpt" ] ; then
		last_mod=$(stat -c %Y "$filename")  # %Y gets the time of last modification in seconds since Epoch
		current_time=$(date +%s)  # Current time in seconds since Epoch
		if (( current_time - last_mod < 3600 )); then
			echo "The file has been modified in the last hour. save it"
			cat cpt | gzip > ~/Documents/cpt.$(date +%Y%m%d).gz
			echo "Removing old ones"
			find ~/Documents -name ctp.* -atime +30 -exec rm {} \; -print
		fi
fi
exit $?
