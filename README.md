# Spreadsheet

Forked from original version of cxrc Originally to fix exit mode. But this original repo doesn't exist any more.

Now it is a fully separated project, to  be modernized, & to fit my own preferences.

This was initially written for Gforth 0.6.* but current version runs on 0.7.* & might in future break 0.6.* compatibility.

# Reason to fork project

- first was missing a real exit to system (_was previously only exit to interpreter_)

- second it was not running on startup of the interpreter (_must run manually main command_) 

- play & understand the code without breaking anything on original author version (_crcx_) 

**NOTE now the current version is more gnu-linux and MacOs-X ANSI color terminal emulators & gforth dependent & might need adjustment to be used on other forth interpreter or operating systems.**

![help](./1.png)

![GUI](./2.png)

# Usage

- 'label    to insert text

- =123      to insert number

- =a1^2     to square a1 cell

- =A3-A4    to input a formula using cell coordinates

- =B5:B13   to sum all of those cells

# Document

**AUTO-DOC**        [doc](./mini-spreadsheet.fs.autodoc.doc)

**MAIN CODE**       [code](./mini-spreadsheet.fs)

**video**           [evolution](https://youtu.be/b4S_Sq9jWBA)

# LICENSE

Chosen [no-license](./LICENSE)

# Last Changes 

```
       _   _   __   
__   _/ | / | / /_  
\ \ / / | | || '_ \ 
 \ V /| |_| || (_) |
  \_/ |_(_)_(_)___/ 
                    
```

# recent changes

-- Thu May 16 05:35:28 PM CEST 2024 --

- [X] - Loading a file now ask to save or not current one

- [X] - Exiting a file now ask to save or not current one

- [X] - Loading a file now displays him without overlay of previous one cells kept intact

-- Tue May 21 01:40:46 PM CEST 2024 --

- [X] - Command line argument (_filename_) can load file at startup now

- [X] - Colors and some bold effects (_ASCII terminal_) are now nicer and more contrasted.

# future 

May not change a lot at that point, may fix or improve existing functions. Concider this version as usable and stable.
